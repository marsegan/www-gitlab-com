---
layout: handbook-page-toc
title: "DM.1.01 - Data Classification Criteria"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# DM.1.01 - Data Classification Criteria

## Control Statement

A data classification policy is in place to define data classes. The policy is available in the Employee Handbook to all internal and external system users and reviewed and approved by management annually. Treatment of confidential data is determined by classification level. 

## Context

This control demonstrates that a data classification policy is currently in place, available, and reviewed annually. It provides classification coverage and handling requirements for various data levels.

## Scope

This control applies to all data managed by GitLab and GitLab employees

## Ownership

* Control Owner: `IT Ops`
* Process owner(s): 
    * IT Ops: `100%`

## Guidance

The policy outlines proper handling and storage requirements for Red, Orange, Yellow and Green data. 

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in this [control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1693).

Examples of evidence an auditor might request to satisfy this control:

* Screenshot or link to the data classification policy
* Screenshot of Version history and issue noting approval by management 

### Policy Reference

[Data Classification Policy](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html)

## Framework Mapping

* SOC2
  * CC3.2
  * CC6.5
