---
layout: handbook-page-toc
title: Training
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Training page! This page is our SSOT page for all training items. 

### Legal

*  N/A

### Finance

*  N/A

### People

*  Learning Sessions
   *  [Live Learning](/handbook/people-group/learning-and-development/#live-learning) Sessions 
      *  [Ally Training](/company/culture/inclusion/ally-training/)
      *  [Inclusion Training](/company/culture/inclusion/inclusion-training/)
      *  [Receiving Feedback](/handbook/people-group/guidance-on-feedback/#responding-to-feedback)
      *  [Communicating Effectively & Responsibly Through Text](/company/culture/all-remote/effective-communication/)
      *  [Compensation Review: Manager Cycle (Compaas)](https://www.youtube.com/watch?v=crkPeOjkqTQ&feature=youtu.be) - just a youtube video
   * [Action Learning](/handbook/people-group/learning-and-development/#action-learning) 
   * [Leadership Forum](/handbook/people-group/learning-and-development/leadership-forum/)  
*  [Certifications & Knowledge Assessments](/handbook/people-group/learning-and-development/certifications/)
   *  [Values](/handbook/values/#gitlab-values-certification)
   *  [Communication](/handbook/communication/#communication-certification)
   *  [Ally](/company/culture/inclusion/ally-training/#ally-certification)
   *  [Inclusion](/company/culture/inclusion/inclusion-training/#inclusion-certification)
   *  [GitLab 101](/handbook/people-group/learning-and-development/certifications/gitlab-101/)
*  [Career Development](/handbook/people-group/learning-and-development/career-development/)
*  [Anti-Harassment](/handbook/people-group/learning-and-development/#common-ground-harassment-prevention-training) 
*  [New Manager Enablement](/handbook/people-group/learning-and-development/manager-development/)
* [People Specialist team and People Experience team training issue](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* [People Specialist team - Contractor Conversion Starter Kit](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* Onboarding/Training Issues
   *  [Onboarding Issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md)
   *  [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/interview_training.md)
   *  [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)
* [Leadership Toolkit](/handbook/people-group/leadership-toolkit/)

### Engineering

*  [Developer Onboarding](/handbook/developer-onboarding/) 

### Product

*  [Product Onboarding](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/PM-onboarding.md)

### Marketing

*  [Marketing Onboarding](https://gitlab.com/gitlab-com/marketing/onboarding/blob/master/.gitlab/issue_templates/non_xdr_function_onboarding.md)

### Field Sales/Sales Enablement

*  [Sales Training Handbook](/handbook/sales/training/)
*  [Sales Quick Start](/handbook/sales/onboarding/sales-learning-path/)
*  [Sales Enablement Sessions](/handbook/sales/training/sales-enablement-sessions/)
*  [Sales Kick Off (SKO)](/handbook/sales/training/SKO/)

### Professional Services

*  [Framework](/handbook/customer-success/professional-services-engineering/framework/)
*  [SKUs](/handbook/customer-success/professional-services-engineering/SKUs/)
