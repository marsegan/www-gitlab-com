---
layout: handbook-page-toc
title: "TAM Playbooks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## TAM Playbooks

TAM's create playbooks to provide a prescriptive methodology to help with customer discussions around certain aspects of GitLab.

Current TAM Playbooks are... *(all links are internal to GitLab only)*
* [Prometheus & Grafana](https://drive.google.com/open?id=1pEu4FxYE8gPAMKGaTDOtdMMfoEKjsfBQ)
* [GitLab Days](https://drive.google.com/open?id=1LrAW0HI-8SiPzgqCfMCy2mf9XYvkWOKG)
* [Executive Business Reviews](https://drive.google.com/open?id=1wQp59jG8uw_UtdNV5vXQjlfC9g5sRD5K)
