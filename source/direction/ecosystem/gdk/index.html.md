---
layout: markdown_page
title: "Product Direction - Enablement"
---

- TOC
{:toc}

This is the product vision for GDK, and it is currently a work in progress.

## Overview

The GDK category is responsible for the GitLab Development Kit, which is the primary method for users to set up a local development environment when contributing to GitLab.

This team is responsible for improving the usability and reliability of the development environment used to build GitLab, the [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit). The GDK is essentially required to locally develop and test the changes, before submitting them upstream to GitLab. It is used by nearly all of the product development organization in some form at GitLab, as well as by any of our community contributors and partners. It is therefore critical to optimize this experience for two reasons: productivity gains made through tool enhancements have compounding returns, and by making it easier to get started we can lower the bar to contributing. Our first focus in this area is to do a thorough evaluation of the challenges and opportunities for the GDK, before recommending a path forward. Ideally we could leverage a containerized build environment, to reduce the amount of initial and on-going work required.

## Themes

Work in progress.